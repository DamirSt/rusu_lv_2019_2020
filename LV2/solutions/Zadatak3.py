import numpy as np
import matplotlib.pyplot as plt

def avg_height(spol):
    if spol == 1:
        return np.dot(visine, spolovi) / np.count_nonzero(spolovi == 1)
    else:
        return -np.dot(visine, spolovi - 1) / np.count_nonzero(spolovi == 0)


spolovi = np.random.randint(2, size = 10)

np.random.seed(42)
visine = np.zeros(10)

visine[spolovi == 0] = np.random.normal(187, 7, len(visine[spolovi == 0]))
visine[spolovi == 1] = np.random.normal(167, 7, len(visine[spolovi == 1]))

print(spolovi)
print(visine)

plt.scatter(np.where(spolovi == 0), visine[spolovi == 0], c = 'r')
plt.scatter(np.where(spolovi == 1), visine[spolovi == 1], c = 'b')

plt.axhline(y = avg_height(0), c = 'red', ls = ':')
plt.axhline(y = avg_height(1), c = 'blue', ls = ':')

plt.show()
