from sklearn import datasets
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage

import numpy as np

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

X = generate_data(500, 1)
print(X)
#plt.scatter(X[:,0], X[:,1])

grouped_data = linkage(X, method='ward')
plt.figure()
dend = dendrogram(grouped_data)

grouped_data = linkage(X, method='centroid')
plt.figure()
dend = dendrogram(grouped_data)

grouped_data = linkage(X, method='median')
plt.figure()
dend = dendrogram(grouped_data)

grouped_data = linkage(X, method='single')
plt.figure()
dend = dendrogram(grouped_data)

grouped_data = linkage(X, method='average')
plt.figure()
dend = dendrogram(grouped_data)

plt.show()

#optimalan broj klastera odredimo tako sto nađemo mjesto prekida i tu vrijdnost odaberemo, u ovom slucaju to bi bila 3 klastera