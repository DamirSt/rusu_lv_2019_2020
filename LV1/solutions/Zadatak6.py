file_name = input('Unesi ime datoteke: ')  
try:
    file_hand = open(file_name)
except:
    print ('File cannot be opened:', file_name)
    exit()

emailAddresses = []
for line in file_hand:
    if 'From: ' in line and '@' in line:
        partsOfLine = line.strip()
        partsOfLine = partsOfLine.split('From: ')
        emailAddresses.append(partsOfLine[1])

hosts = {}
for emailAddress in emailAddresses:   
    emailAdressInParts = emailAddress.split("@")
    host = emailAdressInParts[1]
    if host not in hosts:
        hosts[host] = 0
    hosts[host] += 1


print(emailAddresses[:7])
print(hosts)