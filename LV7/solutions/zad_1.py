import numpy as np
import matplotlib.pyplot as plt
import sklearn.neural_network as nn
from sklearn.preprocessing import StandardScaler
import sklearn.metrics as metrics

def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j) / float(a))
        norm_conf.append(tmp_arr)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
    width = len(c_matrix)
    height = len(c_matrix[0])
    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(x, y),
                        horizontalalignment='center',
                        verticalalignment='center', color='green', size=20)
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')

def generate_data(n):
	
	#prva klasa
	n1 = int(n/2)
	x1_1 = np.random.normal(0.0, 2, (n1,1));
	
	#x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
	x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
	y_1 = np.zeros([n1,1])
	temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
	
	#druga klasa
	n2 = int(n - n/2)
	x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
	y_2 = np.ones([n2,1])
	temp2 = np.concatenate((x_2,y_2),axis = 1)
	data = np.concatenate((temp1,temp2),axis = 0)
	
	#permutiraj podatke
	indices = np.random.permutation(n)
	data = data[indices,:]
	
	return data

np.random.seed(242)
train_data = generate_data(500)
ytrain = train_data[:, 2]
xtrain = train_data[:, 0:2]

np.random.seed(12)
test_data = generate_data(300)
x1learn = test_data[:, 0]
x2learn = test_data[:, 1]
ylearn = test_data[:, 2]
xlearn = test_data[:, 0:2]

scaler = StandardScaler()
scaler.fit(xtrain)
scaler.transform(xtrain)

neural_network = nn.MLPClassifier(hidden_layer_sizes=(20, 20, 10), activation="logistic", alpha=0.001, batch_size=10, max_iter=300)
neural_network.fit(xtrain, ytrain)
ypred = neural_network.predict(xlearn)

C = metrics.confusion_matrix(ylearn, ypred)
TP = C[0][0]
FP = C[0][1]
FN = C[1][0]
TN = C[1][1]

accuracy = (TP + TN) / (TP + TN + FP + FN)
misclassification_rate = 1 - accuracy
precision = TP / (TP + FP)
recall = TP / (TP + FN)
specificity = TN / (TN + FP)

print("ACCURACY: " + str(accuracy))
print("MISCLASSIFICATION RATE: " + str(misclassification_rate))
print("PRECISION: " + str(precision))
print("RECALL: " + str(recall))
print("SPECIFICITY: " + str(specificity))
print(C)
plot_confusion_matrix(C)




f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(test_data[:,0])-0.5:max(test_data[:,0])+0.5:.05, min(test_data[:,1])-0.5:max(test_data[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probab_estimates = neural_network.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probab_estimates, 60, cmap="Greens", vmin=0, vmax=1)
ax_c = f.colorbar(cont)

ax_c.set_ticks([0, 0.25, 0.5, 0.75, 1])

ax.scatter(x1learn, x2learn, c=ylearn)
plt.show()
