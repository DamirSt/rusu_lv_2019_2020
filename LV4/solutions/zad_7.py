import numpy as np
import matplotlib.pyplot as plt 
import pandas as pd  
import seaborn as sns 

from sklearn.metrics import r2_score
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
boston_dataset = load_boston()

boston = pd.DataFrame(boston_dataset.data, columns=boston_dataset.feature_names)
boston.head()

boston['MEDV'] = boston_dataset.target #dodajemo medijan koji nedostaje
boston.isnull().sum()

#Eksplorativna analiza, primjecujemo normalnu distribuciju
sns.set(rc={'figure.figsize':(11.7,8.27)})
sns.distplot(boston['MEDV'], bins=30)
plt.show()

#Prikaz korelacijske matrice svih podataka. Ako je vrijednost blizu 1 onda postoji velika pozitivna korelacija izmedju podataka. Ako je blizu -1 onda postoji negativna korelacija izmedju podataka.
correlation_matrix = boston.corr().round(2)
sns.heatmap(data=correlation_matrix, annot=True)
plt.show()

#S obzirom na matricu, za linerrni model biramo one vrijednosti sa velikom pozitivnom i velikom negativnom korelacijom. U ovom slucaju, biramo "RM" te "LSTAT"

#Pomocu scatter plota mozemo vidjeti kako se one odnose u ovisnosti sa "MEDV"

plt.figure(figsize=(20, 5))

features = ['LSTAT', 'RM']
target = boston['MEDV']

for i, col in enumerate(features):
    plt.subplot(1, len(features) , i+1)
    x = boston[col]
    y = target
    plt.scatter(x, y, marker='o')
    plt.title(col)
    plt.xlabel(col)
    plt.ylabel('MEDV')
plt.show()

#Podjela na dio za ucenje(80%) i dio za testiranje(20%)

X = pd.DataFrame(np.c_[boston['LSTAT'], boston['RM']], columns = ['LSTAT','RM'])
Y = boston['MEDV']

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.2, random_state=5)
print(X_train.shape)
print(X_test.shape)
print(Y_train.shape)
print(Y_test.shape)

#Linearni model

lin_model = LinearRegression()
lin_model.fit(X_train, Y_train)

#Evaluacija dijela za trening
y_train_predict = lin_model.predict(X_train)
rmse = (np.sqrt(mean_squared_error(Y_train, y_train_predict)))
r2 = r2_score(Y_train, y_train_predict)

print("The model performance for training set")
print("--------------------------------------")
print('RMSE is {}'.format(rmse))
print('R2 score is {}'.format(r2))
print("\n")

#Evaluacija dijela za testiranje
y_test_predict = lin_model.predict(X_test)
rmse = (np.sqrt(mean_squared_error(Y_test, y_test_predict)))
r2 = r2_score(Y_test, y_test_predict)

print("The model performance for testing set")
print("--------------------------------------")
print('RMSE is {}'.format(rmse))
print('R2 score is {}'.format(r2))