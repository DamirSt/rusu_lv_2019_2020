import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from sklearn import metrics
from sklearn.preprocessing import PolynomialFeatures

def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

np.random.seed(242)

training_data = generate_data(200)

np.random.seed(12)

test_data = generate_data(100)
test_data_final = np.delete(test_data,[2], 1)

poly = PolynomialFeatures(degree=3, include_bias=False)
data_train_new = poly.fit_transform(training_data[:,0:2])
data_train_new = np.append(data_train_new, training_data[:,2:3], axis=1)
test_data_new = poly.fit_transform(test_data[:,0:2])
test_data_new = np.append(test_data_new, test_data[:,2:3], axis=1)


class_zero_x = []
class_zero_y = []
class_one_x = []
class_one_y = []

for i in range(0, training_data.shape[0]):
    if training_data[i,2] == 1.0:
        class_one_x.append(training_data[i,0])
        class_one_y.append(training_data[i,1])
    else:
        class_zero_x.append(training_data[i,0])
        class_zero_y.append(training_data[i,1])

#plt.scatter(class_one_x, class_one_y, c='red')
#plt.scatter(class_zero_x, class_zero_y, c='blue')
#plt.show()

lin = LogisticRegression()

lin.fit(data_train_new[:, 0:-1], data_train_new[:, -1])
predictions = lin.predict(test_data_new[:, 0:-1])
print("PREDICTIONS:")
print(predictions)
print("ACCURACY SCORE:")
print(lin.score(test_data_new[:, 0:-1], test_data[:,2]))
print("CONFUSION_MATRIX")
print(confusion_matrix(test_data[:,2], predictions))

confusionMatrix = confusion_matrix(test_data[:,2], predictions)

plot_confusion_matrix(confusionMatrix)

tn, fp, fn, tp = confusionMatrix.ravel()
missclarificationRate = 1 - lin.score(test_data_new, test_data[:,2])
precision = metrics.precision_score(test_data[:, 2], predictions)
sensitivity = metrics.recall_score(test_data[:, 2], predictions)
specificity = tn / (tn + fp)

print("MISSCLARIFICATION RATE:")
print(missclarificationRate)
print("PRECISION:")
print(precision)
print("SENSITIVITY:")
print(sensitivity)
print("SPECIFICITY:")
print(specificity)

