import csv
import matplotlib.pyplot as plt
import numpy as np

data = []

with open('C:\\Users\\stipa\\Desktop\\RUSU\\rusu_lv_2019_2020\\LV2\\resources\\mtcars.csv', 'r') as file:
    reader = csv.reader(file)
    for row in file:
        data.append(row.split(','))

data = np.array(data)

mpg = []
hp = []
wt = []

for i in range(data.shape[0]):
    mpg.append(data[i, 1])
    hp.append(data[i, 4])
    wt.append(data[i, 6])

mpg.pop(0)
hp.pop(0)
wt.pop(0)

mpg = np.array(mpg)
hp = np.array(hp)
wt = np.array(wt)

mpg = mpg.astype(np.float)
hp = hp.astype(np.float)
wt = wt.astype(np.float)

plt.plot(mpg, hp, 'ob')
plt.xlabel('Konjska snaga')
plt.ylabel('Potrošnja')

print('Minimalna potrošnja: ', min(mpg))
print('Maksimalna potrošnja: ', max(mpg))
print('Srednja potrošnja: ', (mpg.mean()))

plt.show()