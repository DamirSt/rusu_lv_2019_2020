from sklearn import datasets
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt

import numpy as np

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

X = generate_data(500, 1)
print(X)
#plt.scatter(X[:,0], X[:,1])

kmeans = KMeans(n_clusters=3, init='k-means++', random_state=0)

predicted = kmeans.fit_predict(X)

filtered_cluster1 = X[predicted==0]
filtered_cluster2 = X[predicted==1]
filtered_cluster3 = X[predicted==2]

plt.scatter(filtered_cluster1[:,0], filtered_cluster1[:,1])
plt.scatter(filtered_cluster2[:,0], filtered_cluster2[:,1])
plt.scatter(filtered_cluster3[:,0], filtered_cluster3[:,1])

plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s=300, c='red')

plt.show()