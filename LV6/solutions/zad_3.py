import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

np.random.seed(242)

training_data = generate_data(200)

np.random.seed(12)

test_data = generate_data(100)
test_data_final = np.delete(test_data,[2], 1)

class_zero_x = []
class_zero_y = []
class_one_x = []
class_one_y = []

for i in range(0, training_data.shape[0]):
    if training_data[i,2] == 1.0:
        class_one_x.append(training_data[i,0])
        class_one_y.append(training_data[i,1])
    else:
        class_zero_x.append(training_data[i,0])
        class_zero_y.append(training_data[i,1])

#plt.scatter(class_one_x, class_one_y, c='red')
#plt.scatter(class_zero_x, class_zero_y, c='blue')
#plt.show()

lin = LogisticRegression()

lin.fit(training_data[:,[0,1]], training_data[:,2])
predictions = lin.predict(test_data_final)
print("PREDICTIONS:")
print(predictions)
print("ACCURACY SCORE:")
print(lin.score(test_data_final, test_data[:,2]))
print("CONFUSION_MATRIX")
print(confusion_matrix(test_data[:,2], predictions))

# Dobavljamo parametre
b = lin.intercept_[0]
w1, w2 = lin.coef_.T
# Racunamo intercept i gradient
c = -b/w2
m = -w1/w2

# Plotamo granicu odluke i podatke
xmin, xmax = -5, 5
ymin, ymax = 0, 30
xd = np.array([xmin, xmax])
yd = m*xd + c
plt.plot(xd, yd, 'k', lw=1, ls='--')
plt.fill_between(xd, yd, ymin, color='tab:blue', alpha=0.2)
plt.fill_between(xd, yd, ymax, color='tab:red', alpha=0.2)

plt.scatter(class_one_x, class_one_y, c='red')
plt.scatter(class_zero_x, class_zero_y, c='blue')
plt.xlim(xmin, xmax)
plt.ylim(ymin, ymax)

plt.show()