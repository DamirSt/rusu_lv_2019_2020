import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

def non_func(x):
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

print('Model je oblika y_hat = Theta0 + Theta1 * x')
print('Zad_1: y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')

ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)

plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)

#zad2
arrayOnes = np.ones(len(xtrain))
arrayOnes = arrayOnes[:,np.newaxis]
x_expanded = np.hstack((arrayOnes, xtrain))

theta_ml = np.dot(np.dot(np.linalg.inv(np.dot(np.transpose(x_expanded), x_expanded)), np.transpose(x_expanded)), ytrain)

print('Zad_2: y_hat = ', theta_ml[0], '+', theta_ml[1], '*x')

#zad3
theta_o = [[0.5], [0.5]]
alpha = 0.01
size = len(xtrain)

plt.figure(6)

for i in range(100):
    h_theta = np.dot(x_expanded, theta_o)

    diff = h_theta - ytrain
    diff_transpose = np.transpose(diff)

    dJ0 = (1/size) * np.sum(np.dot(diff_transpose, np.transpose(x_expanded[:, 0])))
    dJ1 = (1/size) * np.sum(np.dot(diff_transpose, np.transpose(x_expanded[:, 1])))

    theta_new0 = theta_o[0] - alpha*dJ0
    theta_new1 = theta_o[1] - alpha*dJ1

    theta_o[0] = theta_new0
    theta_o[1] = theta_new1

    plt.plot([0, 10], [theta_o[0], theta_o[1]*10+theta_o[0]])
    
    print('Zad_3: y_hat = ', theta_o[0], '+', theta_o[1], '*x')

    plt.show()

#Vrijednosti su slicne onima u prvom zadatku ali vece u pocetku. Sa svakom iteracijom vrijednosti se smanjuju
#Povecanjem duljine koraka i vrijednosti brze padaju
