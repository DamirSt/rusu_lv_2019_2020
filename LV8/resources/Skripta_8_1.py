import numpy as np
import joblib
import pickle
from sklearn.datasets import fetch_openml
import sklearn.neural_network as nn
import sklearn.metrics as metrics
import matplotlib.pyplot as plt

def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j) / float(a))
        norm_conf.append(tmp_arr)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
    width = len(c_matrix)
    height = len(c_matrix[0])
    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(x, y),
                        horizontalalignment='center',
                        verticalalignment='center', color='green', size=20)
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

mnist = fetch_openml('mnist_784', version=1, cache=True)
X, y = mnist.data, mnist.target

#print('Got MNIST with %d training- and %d test samples' % (len(X), len(y_test)))
#print('Image size is:')

# rescale the data, train/test split
X = X / 255.
X_train, X_test = X[:60000], X[60000:]
y_train, y_test = y[:60000], y[60000:]

print("Training model...")
# TODO: build youw own neural network using sckitlearn MPLClassifier
mlp_mnist = nn.MLPClassifier(hidden_layer_sizes=(20, 20, 10), activation="logistic", alpha=0.001, batch_size=10, max_iter=300)
mlp_mnist.fit(X_train, y_train)
ypred = mlp_mnist.predict(X_test)


# TODO: evaluate trained NN
C = metrics.confusion_matrix(y_test, ypred)
plot_confusion_matrix(C)

# save NN to disk
filename = "NN_model.sav"
joblib.dump(mlp_mnist, filename)