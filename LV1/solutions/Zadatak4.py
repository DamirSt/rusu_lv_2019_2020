unos = None
total = 0.0
count = 0

svi_unosi = []

while(1):
    unos = input("Unesite neki broj. Za kraj rada upisite 'Done':")
    try:      
        unos = float(unos)
        svi_unosi.append(unos)
        total = total+unos
        count += 1

    except ValueError:
        if(unos == "Done"):
            break
        else:
            print("Molim unesite broj!")

svi_unosi.sort()
print("Unešeno je " + str(count) + " brojeva")
print("Srednja vrijednost svih unosa je " + str(total/count) ) 
print("Najmanji unešeni broj je: " + str(svi_unosi[0]))     
print("Najveći unešeni broj je: " + str(svi_unosi[len(svi_unosi)-1]))     