import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv("C:\\Users\\stipa\\Desktop\\RUSU\\rusu_lv_2019_2020\\LV3\\resources\\mtcars.csv")

#2.1 
plt.figure(figsize=[15,10])
plt.title("2.1")
plt.barh(mtcars.query("cyl==4").car, mtcars.query("cyl==4").mpg, height=0.7, color='r')
plt.barh(mtcars.query("cyl==6").car, mtcars.query("cyl==6").mpg, height=0.7, color='y')
plt.barh(mtcars.query("cyl==8").car, mtcars.query("cyl==8").mpg, height=0.7, color='g')
plt.xlabel("mpg")
plt.legend(["4CYL", "6CYL", "8CYL"])

plt.show()

#2.2
plt.title("2.2")
plt.boxplot([mtcars.query("cyl==4").wt, mtcars.query("cyl==6").wt, mtcars.query("cyl==8").wt], positions=[4,6,8])
plt.xlabel("CYL")
plt.ylabel("Weight [lbs/1000]")

plt.show()

#2.3
plt.title("2.3")
plt.boxplot([mtcars.query("am==0").mpg, mtcars.query("am==1").mpg], positions=[0,1])
plt.xlabel("Transmission(0=manual, 1=automatic)")
plt.ylabel("MPG")

plt.show()

#2.4
plt.title("2.4")
plt.scatter(mtcars.query("am==0").qsec, mtcars.query("am==0").hp, marker='+')   
plt.scatter(mtcars.query("am==1").qsec, mtcars.query("am==1").hp, marker='.')  
plt.ylabel('Snaga(hp)')
plt.xlabel('Ubrzanje(qsec)')
plt.legend(["Automatic","Manual"])
plt.grid()

plt.show()
