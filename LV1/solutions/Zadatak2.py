# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 15:01:18 2020

@author: student
"""
grade = None

while(True):

    try:
        grade = float(input("Unesite broj izmedju 0.1 i 1.0: "))
        
        if grade < 0.1 or grade > 1.0:
            print("Unešeni broj je izvan intervala!")
            continue
        else: 
            break

    except ValueError:
        print("Unesite broj a ne string!")
        
    
if grade < 0.6:    
    print("F")
if grade >= 0.6 and grade < 0.7:
    print("D")
if grade >= 0.7 and grade < 0.8:
    print("C")
if grade >= 0.8 and grade < 0.9:
    print("B")
if grade >= 0.9:
    print("A")
