### Opis dataseta mtcars
Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset

Zadatak vjezbe je bio upoznavanje sa regularnim izrazima, raznim funkcijama numpy library-a i matplotlib funkcijama za plotanje raznih grafova.

U prva dva zadatka iskoristeni su regularni izrazi za ispunjavanje raznih uvjeta pronalazenja specificnih stringova unutar tekst datoteke.
U trecem zadatku iskoristene su razne funkcije za generiranje nasumicnih brojeva te takodjer Gaussove distribucije. Iskoristena je i numpy.dot funkcija kao pomoc pri pronalazenju srednje vrijednosti skupa brojeva bez upotrebe for petlje.
Cetvrti zadatak je poprilicno jednostavan. Jednostavno smo iskoristili numpy.random.randint funkciju da izgeneriramo nasumicne brojeve izmedju 1 i 6 te prikazali rezultat sa grafom pomocu hist() funkcije.
U petom zadatku iskoristena je biblioteka "csv" pomocu koje citamo csv datoteku te onda spremamo procitane vrijednosti za razne specifikacije u zasebne nizove te ih prikazuje pomocu plot() funkcije. Također ispisujemo maksimalnu, minimalnu i srednju vrijednost mpg specifikacije pomocu max(), min() i mean() funckija.
U sestom zadatku jednostavno ucitamo sliku pomocu matplot.image.imread() funkcije te povecavamo vrijednosti zasebnih piksela pri cemu pazimo da ne overflow-amo odredjene piksele.