import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv("C:\\Users\\student\\Desktop\\RUSU\\rusu_lv_2019_2020\\LV3\\resources\\mtcars.csv")

print(mtcars)

mtcars = mtcars.sort_values(by=['mpg'])
print("Prvih 5 automobila sa najvecom potrosnjom su: ")
print(mtcars.car[0:5])

print("3 automobila sa najmanjom potrosnjom sa 8 cilindara su: ")
print(mtcars.query('cyl == 8').car[0:3])

print("Srednja potrosnja automobila sa 6 cilindara je: ")
print(mtcars.query('cyl == 6').mpg.mean())

print("Srednja potrosnja automobila sa 4 cilindra i mase izmedju 2000 i 2200 lbs je: ")
print(mtcars.query('cyl == 4' and 'wt > 2.000' and 'wt < 2.200').mpg.mean())

print("Automobila sa rucnim mjenjacem ima: " + str(mtcars.am.value_counts()[0]))
print("Automobila sa automatskim mjenjacem ima: " + str(mtcars.am.value_counts()[1]))

print("Automobila sa automatskim mjenjacem i snagom preko 100hp ima: " + str(mtcars.query('hp > 100').am.value_counts()[1]))

mtcars.wt = 1000/2.2046226218488*mtcars.wt

print("Tezina svakog automobila u kg iznosi: ")
print(mtcars[['car', 'wt']])