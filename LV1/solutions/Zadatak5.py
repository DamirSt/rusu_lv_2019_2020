file_name = input('Unesi ime datoteke: ')  
try:
    file_hand = open(file_name)
except:
    print ('File cannot be opened:', file_name)
    exit()

values = []
for line in file_hand:
    if 'X-DSPAM-Confidence: ' in line:
        lineInParts = line.split('X-DSPAM-Confidence: ')
        values.append(float(lineInParts[1]))
    
sum = 0.0

for value in values:
    sum += value

print('Average X-DSPAM-Confidence: ', sum / len(values))
